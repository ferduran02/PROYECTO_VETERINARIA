from datetime import datetime

fecha = datetime.now().date()
formafecha = "%d/%m/%y"
ahora = datetime.now()
formato = "%I:%M:%S"
ahora = ("{0}:{1}:{2}").format(ahora.hour, ahora.minute, ahora.second)

usu = {"fer": "12345", "raul": "169905"}
datocliente = {}
mascotas = {}


# DEFINICIONES
def iniciar_sesion(usuario, contra):
    if usuario in usu:
        if usu[usuario] == contra:
            return (True, "___Bienvenido___")
        else:
            return (False, "___Datos invalidos, intente nuevamente___")
    else:
        return (False, "Datos invalidos, intente nuevamente")


class Cliente(object):
    def __init__(self, ced, nombre, direccion, telefono, correo):
        self.ced = ced
        self.nombre = nombre
        self.direccion = direccion
        self.telefono = telefono
        self.correo = correo
        self.listaAanimales = {}



    def __str__(self):
        texto = "Ced: {0} Nommbre: {1} Direccion: {2} Telefono: {3} Correo: {4}".format(self.ced,
                                                                                        self.nombre,
                                                                                        self.direccion,
                                                                                        self.telefono,
                                                                                        self.correo)
        return texto


    def fecha(self):
        while True:
            f = input("Digite la fecha (D/M/A): ")
            if f == formafecha:
                break
            f == datetime.strptime(f, formafecha)
            dia = f.day
            mes = f.moth
            anno = f.year
            fecha = ("Dia: {0}/Mes: {1}/Año: {2}").format(dia, mes, anno)
            return fecha


class Mascota(object):
    def __init__(self, ced, nom, nombre, direccion, telefono, correo, fecha, naci, sexo, tipo, raza, color, peso,
                 obseravaciones):
        self.ced = ced
        self.nom = nom
        self.nombre = nombre
        self.direccion = direccion
        self.telefono = telefono
        self.correo = correo
        self.fecha = fecha
        self.naci = naci
        self.sexo = sexo
        self.tipo = tipo
        self.raza = raza
        self.color = color
        self.peso = peso
        self.observaciones = obseravaciones

    def __str__(self):
        texto = "Ced: {0} Nommbre del dueño: {1} Nombre de la mascota: {2}  Direccion: {3} Telefono: {4} Correo: {5}" \
                "Fecha de ingreso: {6} Fecha de nacimiento: {7} Sexo de la mascota {8} Tipo de mascota: {9}" \
                "Raza: {10} Color: {11} Peso: {12} Observacion: {13}".format(self.ced, self.nom, self.nombre,
                                                                             self.direccion,
                                                                             self.telefono, self.correo, self.fecha,
                                                                             self.naci,
                                                                             self.sexo, self.tipo, self.raza,
                                                                             self.color,
                                                                             self.peso, self.observaciones)
        return texto


menu2 = ("Veterinaria UTN \n"
         "1.Iniciar Sesion\n"
         "2.Salir\n"
         "\n"
         "Seleccione una opcion: ")

menu = ("Veterinaria UTN \n"
        "1.Registre nuevo cliente\n"
        "2.Control de vacunas\n"
        "3.Historial Clinico\n"
        "4.Clientes\n"
        "5.Servicio\n"
        "6.Pago\n"
        "7.Salir"
        "\n"
        "Seleccione una opcion: ")

menu1 = ("Veterinaria UTN \n"
         "1.Registrar mascota\n"
         "2.Eliminar mascota\n"
         "3.Modificar\n"
         "4.Salir"
         "\n"
         "Seleccione una opcion: ")

# MAIN
while True:
    o = int(input(menu2))
    if o == 1:
        usuario = input("Usuario: ")
        contra = input("Contraseña: ")
        log, msj = iniciar_sesion(usuario, contra)
        print(msj)
        while log:
            op = int(input(menu))
            if op == 7:
                log = False
                break
            elif op == 1:
                ced = int(input("Numero de cedula (2-0444-0321): "))
                nombre = input("Nombre completo: ".capitalize())
                fecha = print("Fecha: ", fecha)
                direccion = input("Direccion: ")
                telefono = input("Numero de telefono: ")
                correo = input("Correo electronico: ")
                c = Cliente(ced, nombre, direccion, telefono, correo)
                datocliente.append(c)
                print("___Los datos fueron registrados con exito___")
                while True:
                    opc = int(input(menu1))
                    if opc == 4:
                        break
                    elif opc == 1:
                        tipo = input(
                            "Tipo de mascota: ")  # Menu de tipo de animal(perro,gato,hambster,perico,conejo,caballo,cabra,tortuga)
                        raza = input("Raza: ")
                        ced = int(input("Numero de cedula (2-0444-0321): "))
                        nom = input("Nombre del dueño: ")
                        nombre = input("Nombre de la mascota: ")
                        direccion = input("Direccion: ")
                        telefono = int(input("Numero de telefono: "))
                        correo = input("Correo electronico: ")
                        fecha = input("Fecha de ingreso: ", fecha)  # Extraer actual
                        naci = input("Fecha de nacimiento: ")  # Pasarlo anos
                        sexo = input("Sexo: ")  # Macho, hembra
                        color = input("Color: ")
                        peso = int(input("Peso(KG): "))
                        obseravaciones = input("Observaciones: ")
                        m = Mascota(ced, nom, nombre, direccion, telefono, correo, fecha, naci, sexo, tipo, raza, color,
                                    peso,
                                    obseravaciones)
                        mascotas.append(m)
                        print("Sus datos fueron registrados con exito")
                    elif opc == 2:
                        pass
                    elif opc == 3:
                        pass
            elif op == 2:
                pass
            elif op == 3:
                pass
            elif op == 4:
                if len(datocliente) > 0:
                    for x in datocliente:
                        print(x)
                else:
                    print("No hay clientes registrados")
            elif op == 5:
                pass
            elif op == 6:
                pass
            else:
                print("Opcion inavlida, favor intente nuevamente")
    else:
        break